###
    drop-in replacement for jQuery-UI dialogs
    using bootstrap modals instead

    currently it only supports images
    youtube and/or vimeo videos will be added when needed

    @author Daniel Holzmann <daniel@nnpro.at>
###

# reference jQuery
$ = jQuery

class Dialog
    constructor: (@dialog, @settings) ->
        # create DOM elements
        @createDialog()

        # init bootstrap modal
        @init()

    # creates the necessary DOM elements
    createDialog: =>
        # add a marker element
        if @dialog.parent().length == 0
            @dialog.appendTo 'body'

        @marker = $('<div></div>')
        @dialog.before @marker
        @marker.hide()

        # remember if dialog was visible
        @wasVisible = @dialog.css('display') != 'none'

        @wrapper = $ '<div class="modal hide" tabindex="-1" role="dialog"></div>'
        @wrapper.appendTo 'body'

        # title
        @title = $('<div class="modal-header"></div>').appendTo @wrapper
        @title.html '<button type="button" class="close" data-dismiss="modal">&times;</button>'

        title = @dialog.attr('title') or '&nbsp;'
        @title.append $ "<h3>#{title}</h3>"

        # body
        @body = $('<div class="modal-body clearfix"></div>').appendTo @wrapper
        @dialog.appendTo @body

        # footer
        @footer = $('<div class="modal-footer"></div>').appendTo @wrapper

        @addButton button for button in @settings.buttons

        # ensure that dialog content is not hidden
        @dialog.show()

    # add the given button
    addButton: (data) =>
        prepend = true
        button = $('<button class="btn"></button>')
        button.text data.text
        if data['class']
            button.addClass data['class']
            if data['class'].indexOf('btn-primary') >= 0
                prepend = false

        if prepend
            button.prependTo @footer
        else
            button.appendTo @footer

        if typeof data.click == 'function'
            button.click =>
                data.click.apply @dialog.get(0), arguments

    # init bootstrap modal
    init: =>
        if @settings.modal
            backdrop = 'static'
        else
            backdrop = true
        @wrapper.modal
            backdrop: backdrop
            show: false
            keyboard: @settings.closeOnEscape
            width: @settings.width
            height: @settings.height
            maxHeight: @settings.height

        @wrapper.on 'shown', @shown
        @wrapper.on 'hide', @hide
        @wrapper.on 'hidden', @hidden

        @settings.create.apply @dialog.get(0)

        if @settings.autoOpen then @open()

    # open the dialog
    open: =>
        @wrapper.modal 'show'

    # close the dialog
    close: =>
        @wrapper.modal 'hide'

    # destroy the dialog
    destroy: =>
        # place dialog where it was before
        @marker.after @dialog
        @wrapper.remove()

        if @wasVisible
            @dialog.show()
        else
            @dialog.hide()

        # remove reference to dialog instance
        @dialog.data 'ui-dialog', null

        # delete instance
        delete @

    # event callbacks
    shown: =>
        @settings.open.apply @dialog.get(0)

        # make sure inner events don't mix up with the wrapper
        @dialog.on('show', @preventPropagation).on('hide', @preventPropagation).on('shown', @preventPropagation).on('hidden', @preventPropagation)
        @dialog.find('[data-toggle=tooltip]').each @initTooltip

        # focus first element with autofocus
        @dialog.find('[autofocus=autofocus]').first().focus()

    hide: =>
        @settings.beforeClose.apply @dialog.get(0)
        @settings.afterClose.apply @dialog.get(0)

    hidden: =>
        @settings.close.apply @dialog.get(0)

    preventPropagation: (event) =>
        event.stopPropagation()

    initTooltip: (index, element) =>
        element = $ element

        if typeof element.data('animation') is 'undefined'
            element.data 'animation', false

        element.tooltip()
        element.on('show', @preventPropagation)
        .on('hide', @preventPropagation)
        .on('shown', @preventPropagation)
        .on('hidden', @preventPropagation)




# add plugin
$.fn.extend
    dialog: ->
        # initializing or calling a method?
        if typeof arguments[0] == 'string'
            # method call
            method = arguments[0]
            params = Array.prototype.splice.call(arguments)

            return @each (index, element) ->
                dialog = $(element).data 'ui-dialog'

                if (!dialog)
                    return element

                dialog[method].apply dialog, params
        else
            # initialize
            # default settings
            settings =
                modal: true
                resizable: false
                autoOpen: true
                buttons: []
                closeOnEscape: true
                disabled: false
                close: ->
                open: ->
                create: ->
                beforeClose: ->
                afterClose: ->
                width: 400
                height: null


            settings = $.extend settings, arguments[0]

            return @each (index, element) ->
                dialog = $(element)
                dialog.data 'ui-dialog', new Dialog(dialog, settings)

